const { Todo } = require('../models')

function getAllTodosWithCallback(req, res, callback) {
    Todo.findAll().then(todos => {
        callback(null, todos)
    }).catch(error => {
        callback(error, null)
    })
}

function getTodoByIdWithCallback(req, res, callback) {
    Todo.findOne({ where: { id: req.params.id } }).then(todo => {

        callback(null, todo)
    }).catch(error => {
        callback(error, null)
    })
}


function getAllTodosWithPromise(req, res, next) {
    Todo.findAll().then(todos => {
        res.status(200).json({
            status: true,
            message: 'Todos retrieved!',
            data: { todos }
        })
    })
}

function getTodoByIdWithPromise(req, res, next) {
    Todo.findOne({ where: { id: req.params.id } }).then(todo => {
        if (!todo) {
            res.status(504).json({
                status: false,
                message: 'Todo not found',
                data: {}
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: 'Todo retrieved!',
                data: { todo }
            })

        }

    })
}

function createTodoWithPromise(req, res, next) {
    return Todo.create({
        name: req.body.name,
        desription: req.body.description,
        userId: req.body.userId,
        dueDate: req.body.dueDate
    }).then(function (todo) {
        if (todo) {
            res.send(todo);
        } else {
            res.status(400).send('Error in insert new record');
        }
    });
}

function updateTodoWithPromise(req, res, next) {
    Todo.update(
        {
            name: req.body.name,
            description: req.body.description,
        },
        { where: { userId: req.body.userId } }
    )
        .then(result =>
            res.send(result)
        )
        .catch(err =>
            res.send(err)
        )
}

function deleteTodoWithPromise(req, res, next) {
    Todo.destroy(
        { where: { id: req.body.id } }
    )
        .then(result =>
            res.status(200).json({
                result: result
            })
        )
        .catch(err =>
            res.status(404).json({
                error: err
            })
        )
}

module.exports = { getAllTodosWithCallback, getTodoByIdWithCallback, 
    getAllTodosWithPromise, createTodoWithPromise,getTodoByIdWithPromise,
    updateTodoWithPromise, deleteTodoWithPromise }