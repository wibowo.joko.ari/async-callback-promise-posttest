const { Book, User, Todo } = require('../models')


function getAllUsersWithPromise(req, res, next) {
    User.findAll().then(users => {
        res.status(200).json({
            status: true,
            message: 'Users retrieved!',
            data: { users }
        })
    }).catch(function (error) {
        res.status(504).json({
            status: false,
            message: 'Error Users retrieved!',
            error: error
        })
    })
}

function getUserByIdWithPromise(req, res, next) {
    return User.findOne({ where: { id: req.params.id } }).then(function (thisUser) {
        if (!thisUser) {
            res.status(504).json({
                status: false,
                error: 'User not found'
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: 'User is retrieve!',
                data: { thisUser }
            })
        }

    }).catch(function (error) {
        res.status(504).json({
            status: false,
            error: error
        })
    });

}


function getAllUsersWithCallback(req, res, callback) {
    User.findAll().then(users => {

        callback(null, users)
    }).catch(error => {
        callback(error, null)
    })
}

function getUserByIdWithCallback(req, res, callback) {
    User.findOne({ where: { id: req.params.id } }).then(user => {

        callback(null, user)
    }).catch(error => {
        callback(error, null)
    })
}

function createUserWithCallback(req,res,callback){
    User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }).then(function (user) {
        if (user) {
            callback(null,user);
        } else {
            callback('Error in insert new record','');
        }
    });

}

function createUserWithPromise(req, res, next) {
    return User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }).then(function (user) {
        if (user) {
            res.send(user);
        } else {
            res.status(400).send('Error in insert new record');
        }
    });
}

function updateUserWithPromise(req, res, next) {
    User.update(
        {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        },
        { where: { id: req.body.id } }
    )
        .then(result =>
            res.send(result)
        )
        .catch(err =>
            res.send(err)
        )
}

function deleteUserWithPromise(req, res, next) {
    User.destroy(
        { where: { id: req.body.id } }
    )
        .then(result =>
            res.status(200).json({
                result: result
            })
        )
        .catch(err =>
            res.status(404).json({
                error: err
            })
        )
}

module.exports = {
    getAllUsersWithPromise,
    getAllUsersWithCallback,
    getUserByIdWithCallback,
    getUserByIdWithPromise,
    createUserWithPromise,
    createUserWithCallback,
    updateUserWithPromise,
    deleteUserWithPromise

} 
