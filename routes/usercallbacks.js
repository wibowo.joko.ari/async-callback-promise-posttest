const express = require('express')
const userController = require('../controllers/userController')
const router = express.Router()

router.get('/', function (req, res, next) {
    try {
        userController.getAllUsersWithCallback(req, res, function (error, users) {
            if (!error) {
                res.send({
                    status: true,
                    message: "OK",
                    data: users
                })
            }
            else {
                res.send({
                    status: false,
                    message: error
                });
            }
        })

    } catch (error) {
        res.status(504).send({
            status: false,
            message: error
        });
    }
});

router.get('/user/:id', function (req, res, next) {
    try {
        userController.getUserByIdWithCallback(req, res, function (error, user) {
            if (!error) {
                res.send({
                    status: true,
                    message: "OK",
                    data: user
                });
            }
            else {
                res.send({
                    status: false,
                    message: error
                });
            }
        })

    } catch (error) {
        res.status(504).send({
            status: false,
            message: error
        });

    }
});


router.post('/User', function (req, res, next) {
    try {
        userController.createUserWithCallback(req, res, function (error, user) {
            if (!error) {
                res.send({
                    status: true,
                    message: "OK",
                    data: user
                });
            }
            else {
                res.status(504).send({
                    status: false,
                    message: error
                });
            }
        });

    } catch (error) {
        res.status(504).send({
            status: false,
            message: error
        });

    }

});






module.exports = router;