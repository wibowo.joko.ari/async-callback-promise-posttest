const express = require('express')
const { Book, User, Todo } = require('../models')
const todoController = require('../controllers/todoController')
const router = express.Router()


router.get('/', function (req, res, next) {
    try {
        todoController.getAllTodosWithCallback(req, res, function (error, todos) {
            if (!error) {
                res.send({
                    status: true,
                    message: "OK",
                    data: todos
                })
            }
            else {
                res.send({
                    status: false,
                    message: "NOK"
                });
            }
        })

    } catch (error) {
        res.status(504).send({
            status: false,
            message: error
        });
    }
});

router.get('/todo/:id', function (req, res, next) {
    try {
        todoController.getTodoByIdWithCallback(req, res, function (error,todo) {
            if (!error) {
                res.send({
                    status: true,
                    message: "OK",
                    data: todo
                })
            }
            else {
                res.send({
                    status: false,
                    message: "NOK"
                });
            }
        })

    } catch (error) {
        res.status(504).send({
            status: false,
            message: error
        });
    }
});


router.post('/Todo', function (req, res, next) {
    return Todo.create({
        name: req.body.name,
        desription: req.body.description,
        userId: req.body.userId,
        dueDate: req.body.dueDate
    }).then(function (todo) {
        if (todo) {
            res.send(todo);
        } else {
            res.status(400).send('Error in insert new record');
        }
    });
});

router.put('/Todo', function (req, res, next) {
    Todo.update(
        {
            name: req.body.name,
            description: req.body.description,
        },
        { where: { userId: req.body.userId } }
    )
        .then(result =>
            res.send(result)
        )
        .catch(err =>
            res.send(err)
        )
});


router.delete('/Todo', function (req, res, next) {
    Todo.destroy(
        { where: { id: req.body.id } }
    )
        .then(result =>
            res.status(200).json({
                result: result
            })
        )
        .catch(err =>
            res.status(404).json({
                error: err
            })
        )
});

module.exports = router;