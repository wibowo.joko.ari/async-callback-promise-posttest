const express = require('express')
const userController = require('../controllers/userController')
const router = express.Router()

router.get('/', userController.getAllUsersWithPromise);

router.get('/user/:id', userController.getUserByIdWithPromise);

router.post('/User', userController.createUserWithPromise);

router.put('/User', userController.updateUserWithPromise);

router.delete('/User', userController.deleteUserWithPromise);

module.exports = router;