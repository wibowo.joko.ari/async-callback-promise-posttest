const express = require('express')
const todoController = require('../controllers/todoController')
const router = express.Router()

router.get('/', todoController.getAllTodosWithPromise);

router.get('/todo/:id', todoController.getTodoByIdWithPromise);

router.post('/Todo', todoController.createTodoWithPromise);

router.put('/Todo', todoController.updateTodoWithPromise);

router.delete('/Todo', todoController.deleteTodoWithPromise);

module.exports = router;