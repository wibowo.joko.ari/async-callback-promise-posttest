const express = require('express')
const { Book, User, Todo } = require('../models')
const userController = require('../controllers/userController')
const router = express.Router()

router.get('/', async function (req, res, next) {
    try {
        let allUsers = await User.findAll()
        await res.status(200).json({
            status: true,
            message: 'Users retrieved!',
            data: { allUsers }
        });

    } catch (error) {
        return res.status(500).send();
    }
});

router.get('/user/:id', async function (req, res, next) {
    try {
        let thisUser = await User.findOne({ where: { id: req.params.id } })
        let user = await thisUser;

        if (!user) {
            res.status(200).json({
                status: false,
                message: 'User is not retrieve!',
                data: { thisUser }
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: 'User retrieved!',
                data: { thisUser }
            })
        }
    } catch (error) {
        return res.status(500).send();
    }
});


router.post('/User', async function (req, res, next) {
    try {
        let create = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        });

        if (!create) {
            res.status(400).send('Error in insert new record');
        }
        else {
            res.send(create);
        }

    } catch (error) {
        res.status(504).send(error);
    }

});

router.put('/User', async function (req, res, next) {
    try {
        let update = await User.update(
            {
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            },
            { where: { id: req.body.id } }
        )


        if (!update) {
            res.status(400).send('Error in updating record');
        }
        else {
            res.send(update);
        }

    } catch (error) {
        res.status(504).send(error);
    }
});

router.delete('/user', async function (req, res, next) {
    try {
        let destroy = await User.destroy(
            { where: { id: req.body.id } }
        )

        if (!destroy) {
            res.status(400).json({
                error: 'Record not found'
            })
        }
        else {
            res.status(200).json({
                message: 'Success deleting record'
            })
        }

    } catch (error) {
        res.status(504).json({
            error: error
        })
    }

});


module.exports = router;