
const express = require('express')
const {  Todo } = require('../models')
const todoController = require('../controllers/todoController')
const router = express.Router()



router.get('/', async function (req, res, next) {
    try {
        let allTodos = await Todo.findAll()
        await res.status(200).json({
            status: true,
            message: 'Todos retrieved!',
            data: { allTodos }
        });

    } catch (error) {
        return res.status(500).send();
    }
});


router.get('/Todo/:id', async function (req, res, next) {
    try {
        let thisTodo = await Todo.findOne({ where: { id: req.params.id } })
        let todo = await thisTodo;

        if (!todo) {
            res.status(200).json({
                status: false,
                message: 'Todo is not retrieve!',
                data: { todo }
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: 'Todo retrieved!',
                data: { todo }
            })
        }
    } catch (error) {
        return res.status(500).send();
    }
});

router.post('/Todo', async function (req, res, next) {
    try {
        let create = await Todo.create({
            name: req.body.name,
            desription: req.body.description,
            userId: req.body.userId,
            dueDate: req.body.dueDate
        });

        if (!create) {
            res.status(400).send('Error in insert new record');
        }
        else {
            res.send(create);
        }

    } catch (error) {
        res.status(504).send(error);
    }

});

router.put('/Todo', async function (req, res, next) {
    try {
        let update = await Todo.update(
            {
                name: req.body.name,
                description: req.body.description,
            },
            { where: { id: req.body.id } }
        )


        if (!update) {
            res.status(400).send('Error in updating record');
        }
        else {
            res.send(update);
        }

    } catch (error) {
        res.status(504).send(error);
    }
});


router.delete('/Todo', async function (req, res, next) {
    try {
        let destroy = await Todo.destroy(
            { where: { id: req.body.id } }
        )

        if (!destroy) {
            res.status(400).json({
                error: 'Record not found'
            })
        }
        else {
            res.status(200).json({
                message: 'Success deleting record'
            })
        }

    } catch (error) {
        res.status(504).json({
            error: error
        })
    }

});

module.exports = router;


