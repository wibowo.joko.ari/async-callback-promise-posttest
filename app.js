const express = require('express')
var cookieParser = require('cookie-parser');
const { Book, User, Todo } = require('./models')
const app = express()
const port = 5000
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var userCallbacksRouter = require('./routes/usercallbacks');
var userAsyncsRouter = require('./routes/userasyncs');
var todosRouter = require('./routes/todos');
var todoCallbacksRouter = require('./routes/todocallbacks');
var todoAsyncsRouter = require('./routes/todoasyncs');

app.use(express.json())
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);

app.use('/users', usersRouter);
app.use('/usercallbacks', userCallbacksRouter);
app.use('/userasyncs', userAsyncsRouter);

app.use('/todos', todosRouter);
app.use('/todocallbacks', todoCallbacksRouter);
app.use('/todoasyncs', todoAsyncsRouter);


app.listen(port, () => console.log('listening: ' + port))

//module.exports = app;